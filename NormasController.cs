using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PergamumGedWebInterface.Pergamum;

namespace PergamumGedWebInterface
{
    [Produces("application/json")]
    [ApiController]
    [Route("normas")]
    public class NormasController
    {
        private readonly IPergamumApi _pergamumApi;

        public NormasController(IPergamumApi pergamumApi)
        {
            _pergamumApi = pergamumApi;
        }

        [HttpGet]
        public async Task<IList<PergamumNorma>> GetNormas(
            [FromQuery] [Required] string busca,
            [FromQuery] [Required] string codigoCategoria,
            [FromQuery] int page,
            [FromQuery] int pageSize
        )
        {
            const string uriSearch = "https://www.gedweb.com.br/services/api/v1/search/";
            var result = await _pergamumApi.GedPesquisa(busca, uriSearch + codigoCategoria, page, pageSize);
            foreach (var norma in result) norma.Titulo = Regex.Replace(norma.Titulo, @"<([^>]+)>", "");
            return result;
        }
    }
}

