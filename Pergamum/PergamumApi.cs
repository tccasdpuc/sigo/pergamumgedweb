using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NiL.JS.Core;

namespace PergamumGedWebInterface.Pergamum
{
    public class PergamumApi : IPergamumApi
    {
        private readonly HttpClient _httpClient;
        private readonly ILogger<PergamumApi> _logger;
        private readonly PergamumSettings _settings;
        private static bool _initDone = false; 

        public PergamumApi(HttpClient httpClient, PergamumSettings settings, ILogger<PergamumApi> logger)
        {
            _httpClient = httpClient;
            _settings = settings;
            _logger = logger;
        }

        private async Task Init()
        {
            await Index();
            await MostraLogin();
            await ValidaAcessoNovo();
        }

        public async Task Index()
        {
            _logger.LogDebug("Index");
            var requestUri = _settings.Url;
            await _httpClient.GetAsync(requestUri);
        }

        public async Task MostraLogin()
        {
            _logger.LogDebug("MostraLogin");
            var requestUri = _settings.Url;
            var request = new PergamumRequest("ajax_mostra_login");
            request.AddArg("3");
            using var content = request.ToHttpContent();
            await _httpClient.PostAsync(requestUri, content);
        }

        public async Task ValidaAcessoNovo()
        {
            _logger.LogDebug("ValidaAcessoNovo");
            var requestUri = _settings.Url;
            var settings = _settings;
            var login = settings.Login;
            var password = settings.Password;
            var request = new PergamumRequest("ajax_valida_acesso_novo");
            request.AddArg(login);
            request.AddArg(password);
            request.AddArg("3");
            using var content = request.ToHttpContent();
            await _httpClient.PostAsync(requestUri, content);
            _initDone = true;
        }

        public async Task<IList<PergamumCategory>> GedProdutos(string search)
        {
            if (_initDone == false) await Init();
            _logger.LogDebug($"GedProdutos.({search})");
            var requestUri = _settings.Url;
            var request = new PergamumRequest("ajax_ged_produtos");
            request.AddArg(search);
            using var content = request.ToHttpContent();
            var response = await _httpClient.PostAsync(requestUri, content);
            var result = await response.Content.ReadAsStringAsync();
            if (result[0] == '-') return null;
            var code = result.Substring(2);
            var ctx = new Context();
            ctx.Eval(code);
            var jsValue = ctx.GetVariable("res");
            return jsValue.AsList<PergamumCategory>();
        }

        public async Task<IList<PergamumNorma>> GedPesquisa(string search, string uri, int page, int pageSize)
        {
            if (_initDone == false) await Init();
            _logger.LogDebug($"GedPesquisa({search},{uri},{page},{pageSize})");
            var requestUri = _settings.Url;
            var request = new PergamumRequest("ajax_ged_pesquisa");
            request.AddArg(search);
            request.AddArg(uri);
            request.AddArg(page.ToString());
            request.AddArg(pageSize.ToString());
            using var content = request.ToHttpContent();
            var response = await _httpClient.PostAsync(requestUri, content);
            var result = await response.Content.ReadAsStringAsync();
            if (result[0] == '-') return null;
            var code = result.Substring(2);
            var ctx = new Context();
            ctx.Eval(code);
            var jsValue = ctx.GetVariable("res");
            return jsValue.AsList<PergamumNorma>();
        }

        public async Task<LinkVisualizacao> GedConsultaItem(string item)
        {
            if (_initDone == false) await Init();
            _logger.LogDebug($"GedConsultaItem({item})");
            var requestUri = _settings.Url;
            var request = new PergamumRequest("ajax_ged_consulta_item");
            request.AddArg("LinkVisualizacao");
            request.AddArg(item);
            using var content = request.ToHttpContent();
            var response = await _httpClient.PostAsync(requestUri, content);
            var result = await response.Content.ReadAsStringAsync();
            var code = result.Substring(2);
            var ctx = new Context();
            ctx.Eval(code);
            var jsValue = ctx.GetVariable("res");
            if (result[0] != '-')
            {
                var res = jsValue.As<LinkVisualizacao>();
                if (res != null) return res;
            }
            _logger.LogWarning(code);
            var error = jsValue.As<PergamumError>();
            if(error != null) throw new Exception(error.Error);
            return null;
        }
    }
}