using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace PergamumGedWebInterface.Pergamum
{
    public class PergamumRequest
    {
        public PergamumRequest(string rs)
        {
            Rs = rs;
            RsArgs = new List<string>();
        }

        public string Rs { get; set; }
        public string Rst { get; set; }
        public string RsRnd { get; set; }
        public IList<string> RsArgs { get; set; }

        public void AddArg(string arg)
        {
            RsArgs.Add(HttpUtility.UrlEncode(arg));
        }

        public HttpContent ToHttpContent()
        {
            RsRnd = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds().ToString();
            var collection = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("rs", Rs),
                new KeyValuePair<string, string>("rst", Rst),
                new KeyValuePair<string, string>("rsrnd", RsRnd)
            };
            collection.AddRange(RsArgs.Select(rsArg => new KeyValuePair<string, string>("rsargs[]", rsArg)));
            var body = string.Join('&', collection.Select(kp => $"{kp.Key}={kp.Value}"));
            var content = new StringContent(body);
            content.Headers.Clear();
            content.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            return content;
        }
    }
}