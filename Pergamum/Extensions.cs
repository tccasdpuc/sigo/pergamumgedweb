using System;
using System.Collections.Generic;
using System.Linq;
using NiL.JS.Core;

namespace PergamumGedWebInterface.Pergamum
{
    internal static class Extensions
    {
        public static T As<T>(this JSValue jsValue) where T : class, new()
        {
            if (jsValue.IsNull == false && jsValue.ValueType == JSValueType.Object)
            {
                var type = typeof(T);
                var t = new T();
                var properties = type.GetProperties();
                var keyValuePairs = jsValue.ToList();
                foreach (var (key, value) in keyValuePairs)
                {
                    var property = properties.FirstOrDefault(p =>
                        string.Equals(p.Name, key, StringComparison.CurrentCultureIgnoreCase));
                    if (property == null) continue;
                    if (value.Value.GetType() == property.PropertyType)
                        property.SetValue(t, value.Value);
                }

                return t;
            }

            return null;
        }

        public static IList<T> AsList<T>(this JSValue jsValue) where T : class, new()
        {
            if (jsValue.Exists && jsValue.ValueType == JSValueType.Object)
            {
                var list = new List<T>();
                var keyValuePairs = jsValue.ToList();
                foreach (var (key, value) in keyValuePairs) list.Add(value.As<T>());

                return list;
            }

            return null;
        }
    }
}