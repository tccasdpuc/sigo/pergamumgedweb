using System.Collections.Generic;
using System.Threading.Tasks;

namespace PergamumGedWebInterface.Pergamum
{
    public interface IPergamumApi
    {
        public Task Index();
        public Task MostraLogin();
        public Task ValidaAcessoNovo();
        public Task<IList<PergamumCategory>> GedProdutos(string search);
        public Task<IList<PergamumNorma>> GedPesquisa(string search, string uri, int page, int pageSize);
        public Task<LinkVisualizacao> GedConsultaItem(string item);
    }
}