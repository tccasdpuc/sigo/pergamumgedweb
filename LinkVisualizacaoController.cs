using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PergamumGedWebInterface.Pergamum;

namespace PergamumGedWebInterface
{
    [Produces("application/json")]
    [ApiController]
    [Route("linkVisualizacao")]
    public class LinkVisualizacaoController
    {
        private readonly IPergamumApi _pergamumApi;

        public LinkVisualizacaoController(IPergamumApi pergamumApi)
        {
            _pergamumApi = pergamumApi;
        }

        [HttpGet]
        public async Task<LinkVisualizacao> GetinkVisualizacao([FromQuery] [Required] string item)
        {
            var result = await _pergamumApi.GedConsultaItem(item.Replace(' ','+'));
            return result;
        }
    }
}