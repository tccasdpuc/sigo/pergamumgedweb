using System;
using System.IO;
using System.Net.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NLog.Extensions.Logging;
using PergamumGedWebInterface.Pergamum;
using Polly;

namespace PergamumGedWebInterface
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<PergamumSettings>(Configuration.GetSection(PergamumSettings.SectionKey));
            services.AddSingleton(PergamumSettings.Factory);

            services.AddLogging(builder =>
            {
                builder.ClearProviders();
                builder.AddNLog(Configuration);
            });

            services
                .AddHttpClient<IPergamumApi, PergamumApi>(ConfigurePergamumApiHttpClient)
                .AddTransientHttpErrorPolicy(ConfigurePolicy);

            services.AddControllers();

            services.AddSwaggerGen();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseExceptionHandler("/error");

            app.UseRouting();

            //app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Pergamum GEDWeb API"); });
        }

        private static void ConfigurePergamumApiHttpClient(IServiceProvider serviceProvider, HttpClient httpClient)
        {
            httpClient.DefaultRequestHeaders.Add("User-Agent", "PergamumGedWebInterface");
        }

        private static IAsyncPolicy<HttpResponseMessage> ConfigurePolicy(PolicyBuilder<HttpResponseMessage> builder)
        {
            return builder.WaitAndRetryAsync(new[]
            {
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(5),
                TimeSpan.FromSeconds(5)
            });
        }
    }

    public class PergamumSettings
    {
        public const string SectionKey = "Pergamum";
        public string Url { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public static PergamumSettings Factory(IServiceProvider serviceProvider)
        {
            var options = serviceProvider.GetRequiredService<IOptions<PergamumSettings>>();
            var url = Environment.GetEnvironmentVariable("PERGAMUM_URL");
            var login = Environment.GetEnvironmentVariable("PERGAMUM_LOGIN");
            var password = Environment.GetEnvironmentVariable("PERGAMUM_PASSWORD");
            return new PergamumSettings
            {
                Url = url ?? options.Value.Url,
                Login = login ?? options.Value.Login,
                Password = password ?? options.Value.Password
            };
        }
    }
}