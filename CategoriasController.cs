using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PergamumGedWebInterface.Pergamum;

namespace PergamumGedWebInterface
{
    [Produces("application/json")]
    [ApiController]
    [Route("categorias")]
    public class CategoriasController
    {
        private readonly IPergamumApi _pergamumApi;

        public CategoriasController(IPergamumApi pergamumApi)
        {
            _pergamumApi = pergamumApi;
        }

        [HttpGet]
        public async Task<IList<Categoria>> GetCategorias([FromQuery] [Required] string busca)
        {
            var result = await _pergamumApi.GedProdutos(busca);
            return result?.Select(Categoria.FromPergamumCategory).ToList();
        }
    }

    public class Categoria
    {
        public Categoria(string nome, string codigo, int totalRegistrosEncontrados)
        {
            Nome = nome;
            Codigo = codigo;
            TotalRegistrosEncontrados = totalRegistrosEncontrados;
        }

        public string Nome { get; set; }
        public string Codigo { get; set; }
        public int TotalRegistrosEncontrados { get; set; }

        public static Categoria FromPergamumCategory(PergamumCategory pergamumCategory)
        {
            const string gedWebUriSearch = "https://www.gedweb.com.br/services/api/v1/search/";
            var codigo = pergamumCategory.UriSearch.Replace(gedWebUriSearch, "");
            return new Categoria(pergamumCategory.Nome,codigo,pergamumCategory.TotalRegistrosEncontrados);
        }
    }
}